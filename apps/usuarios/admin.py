from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from . import models as m

class CustomUserAdmin(UserAdmin):
    fieldsets = UserAdmin.fieldsets + (
            # (None, {'fields': ('extra',)}),
    )
 
admin.site.register(m.Usuario, CustomUserAdmin)
