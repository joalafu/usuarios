# -*- coding:utf-8 -*-
from django import forms
from django.contrib.auth.forms import UserCreationForm
from apps.usuarios.models import Usuario
from bootstrap_modal_forms.mixins import PopRequestMixin, CreateUpdateAjaxMixin

class CreateUserForm(PopRequestMixin, CreateUpdateAjaxMixin, UserCreationForm):
    class Meta:
        model = Usuario
        fields = [
            'username',
            'last_name',
            'first_name',
            'email',
            'password1',
            'password2',
        ]
        error_messages = {
            'username': {'unique': 'No puede utilizar este Nombre de usuario.'},
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({'class': 'form-control input'})

class UpdateUserForm(PopRequestMixin, CreateUpdateAjaxMixin, forms.ModelForm):
    class Meta:
        model = Usuario
        fields = [
            'username',
            'last_name',
            'first_name',
            'email',
        ]
        error_messages = {
            'username': {'unique': 'No puede utilizar este Nombre de usuario.'},
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({'class': 'form-control input'})

class UserBajaLogicaForm(PopRequestMixin, CreateUpdateAjaxMixin, forms.ModelForm):
    class Meta:
        model = Usuario
        fields = []