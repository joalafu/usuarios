from django.db import models
from django.contrib.auth.models import AbstractUser

class Usuario(AbstractUser):
    REQUIRED_FIELDS = ['email']

    def __str__(self):
        return '%s' % self.get_full_name()