from django.urls import path, re_path

from . import views

app_name = 'usuarios'

urlpatterns = [
    path('', views.UsuariosView.as_view(), name="listar"),
    path('crear/', views.CrearUsuarioView.as_view(), name="crear"),
    re_path('(?P<pk>\d+)/modificar/', views.ModificarUsuarioView.as_view(), name="modificar"),
    re_path('(?P<pk>\d+)/desactivar/', views.DesactivarUsuarioView.as_view(), name="desactivar"),
]

