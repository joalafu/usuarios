from django.urls import reverse_lazy
from django.views.generic.list import ListView
from apps.usuarios.models import Usuario
from bootstrap_modal_forms.mixins import PassRequestMixin, DeleteAjaxMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic.edit import CreateView, UpdateView
from apps.usuarios import forms

class UsuariosView(ListView):
    model = Usuario
    template_name = 'usuarios.html'

class CrearUsuarioView(PassRequestMixin, SuccessMessageMixin, CreateView):
    model = Usuario
    form_class = forms.CreateUserForm
    template_name =  'agregar_modificar_modal_columnas.html'
    success_url = reverse_lazy('usuarios:listar')
    success_message = 'Datos guardados exitosamente.'

    def get_context_data(self, **kwargs):
        contexto = super().get_context_data(**kwargs)
        contexto['title'] = 'Agregar Usuario'
        return contexto
        
    def form_valid(self, form):
        usuario = form.save(commit=False)
        usuario.is_staff = True
        usuario.is_superuser = True
        return super().form_valid(form)

class ModificarUsuarioView(PassRequestMixin, SuccessMessageMixin, UpdateView):
    model = Usuario
    form_class = forms.UpdateUserForm
    template_name = 'agregar_modificar_modal_columnas.html'
    success_url = reverse_lazy('usuarios:listar')
    success_message = 'Datos guardados exitosamente.'

    def get_context_data(self, **kwargs):
        contexto = super().get_context_data(**kwargs)
        contexto['title'] = 'Modificar Usuario'
        return contexto

class DesactivarUsuarioView(PassRequestMixin, SuccessMessageMixin, UpdateView):
    model = Usuario
    form_class = forms.UserBajaLogicaForm
    template_name = 'eliminar_modal.html'
    success_message = 'Usuario desactivado exitosamente.'
    success_url =  reverse_lazy('usuarios:listar')

    def get_context_data(self, **kwargs):
        contexto = super().get_context_data(**kwargs)
        contexto['modelo_nombre'] = 'Usuario'
        return contexto

    def form_valid(self, form):
        usuario = form.save(commit=False)
        usuario.is_active = False
        return super().form_valid(form)